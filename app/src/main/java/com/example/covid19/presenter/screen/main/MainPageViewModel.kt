package com.example.covid19.presenter.screen.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.covid19.domain.usecase.CovidUseCase
import com.example.covid19.presenter.model.CovidTotal
import com.example.covid19.data.dto.DataFromToRequest
import com.example.covid19.data.mapper.Mapper
import com.example.covid19.presenter.utils.Event
import com.example.covid19.presenter.utils.MyConstants.YYYY_MM_DD
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class MainPageViewModel @Inject constructor(private val useCase: CovidUseCase) : ViewModel() {

    private var _worldTotal = MutableLiveData<CovidTotal>()
    val worldTotal: LiveData<CovidTotal> = _worldTotal

    private var _covidUzb = MutableLiveData<CovidTotal>()
    val covidUzb: LiveData<CovidTotal> = _covidUzb

    private var _message = MutableLiveData<Event<String>>()
    val message: LiveData<Event<String>> = _message

    fun getWorldTotal() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val data = useCase.getWorldStatistics()
                _worldTotal.postValue(Mapper.globalToCovidTotal(data))

            } catch (e: Exception) {
                _message.postValue(Event(e.message!!))
            } catch (e: HttpException) {
                _message.postValue(Event(e.message()))
            }
        }
    }

    fun getUzbTotal(dataRequest: DataFromToRequest) {
        if (dataRequest.from == "fromT00:00:00Z" || dataRequest.to == "toT00:00:00Z"){
            _message.postValue(Event("You have to select both times! "))
            return
        }
        val date1 = SimpleDateFormat(YYYY_MM_DD, Locale("uz")).parse(dataRequest.from)
        val date2 = SimpleDateFormat(YYYY_MM_DD, Locale("uz")).parse(dataRequest.to)

        if (date1 < date2 && date2 <= Date()){
            viewModelScope.launch(Dispatchers.IO) {
                try {
                    val ls = useCase.getUzbData(dataRequest)
                    _covidUzb.postValue(Mapper.listUzbDataToCovidTotal(ls))
                } catch (e: Exception) {
                    _message.postValue(Event(e.message!!))
                } catch (e: HttpException) {
                    _message.postValue(Event(e.message()))
                }
            }
        }
        else{
            _message.postValue(Event("The time is incorrect!"))
        }
    }

    fun getUzbSingle() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val uzbCovid19 = useCase.getUzbDataSingle()
                _covidUzb.postValue(Mapper.uzbSingleDataToCovidTotal(uzbCovid19))
            } catch (e: Exception) {
                _message.postValue(Event(e.message!!))
            } catch (e: HttpException) {
                _message.postValue(Event(e.message()))
            }
        }
    }
}