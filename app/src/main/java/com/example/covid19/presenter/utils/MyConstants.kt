package com.example.covid19.presenter.utils

object MyConstants {
    const val SHARED_PREF = "SHARED_PREF"
    const val BASE_URL = "https://api.covid19api.com/"
    const val YYYY_MM_DD = "yyyy-MM-dd"
}