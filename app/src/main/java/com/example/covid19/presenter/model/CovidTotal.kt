package com.example.covid19.presenter.model

import com.google.gson.annotations.SerializedName

data class CovidTotal(
    @SerializedName("TotalConfirmed")
    val totalConfirmed: Int,
    @SerializedName("TotalDeaths")
    val totalDeaths: Int,
    @SerializedName("TotalRecovered")
    val totalRecovered: Int
)