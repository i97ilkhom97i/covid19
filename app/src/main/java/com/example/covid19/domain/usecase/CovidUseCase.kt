package com.example.covid19.domain.usecase

import com.example.covid19.data.repository.CovidRepository
import com.example.covid19.data.dto.DataFromToRequest
import com.example.covid19.data.dto.GlobalResponse
import com.example.covid19.data.dto.Covid19Response
import javax.inject.Inject

class CovidUseCase @Inject constructor(private val covidRepository: CovidRepository) {

    suspend fun getWorldStatistics(): GlobalResponse {
        return covidRepository.getWorldStatistics()
    }

    suspend fun getUzbData(dataRequest: DataFromToRequest): List<Covid19Response> {
        return covidRepository.getUzbData(dataRequest)
    }

    suspend fun getUzbDataSingle(): Covid19Response {
        return covidRepository.getUzbDataSingle()
    }

}