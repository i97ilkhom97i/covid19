package com.example.covid19.domain.repository

import com.example.covid19.data.dto.DataFromToRequest
import com.example.covid19.data.dto.GlobalResponse
import com.example.covid19.data.dto.Covid19Response

interface Covid {
    suspend fun getWorldStatistics() : GlobalResponse
    suspend fun getUzbData(dataRequest: DataFromToRequest) : List<Covid19Response>
    suspend fun getUzbDataSingle() : Covid19Response
}