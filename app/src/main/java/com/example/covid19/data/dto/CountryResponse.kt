package com.example.covid19.data.dto

import com.google.gson.annotations.SerializedName

data class CountryResponse(
    @SerializedName("Country")
    val country: String,
    @SerializedName("CountryCode")
    val countryCode: String,
    @SerializedName("Date")
    val date: String,
    @SerializedName("ID")
    val ID: String,
    val baseStatistics: BaseStatistics,
    @SerializedName("Slug")
    val slug: String,
    @SerializedName("Premium")
    val premiumResponse: PremiumResponse
)