package com.example.covid19.data.api

import com.example.covid19.data.dto.MainDataResponse
import com.example.covid19.data.dto.Covid19Response
import retrofit2.http.*

interface CovidAPI {

    @GET("summary")
    suspend fun getCountries() : MainDataResponse

    @GET("country/uzbekistan")
    suspend fun getUzbData(@Query("from") data1:String, @Query("to") data2:String) : List<Covid19Response>

    @GET("live/country/uzbekistan/status/confirmed")
    suspend fun getUzbSingleData() : List<Covid19Response>
}