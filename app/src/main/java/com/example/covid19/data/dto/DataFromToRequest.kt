package com.example.covid19.data.dto

data class DataFromToRequest(
    val from:String,
    val to:String
)