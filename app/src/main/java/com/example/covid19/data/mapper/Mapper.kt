package com.example.covid19.data.mapper

import com.example.covid19.data.dto.GlobalResponse
import com.example.covid19.data.dto.Covid19Response
import com.example.covid19.presenter.model.CovidTotal

object Mapper {

    fun globalToCovidTotal(globalResponse: GlobalResponse): CovidTotal {
        val death = globalResponse.totalDeaths
        val all = globalResponse.totalConfirmed
        val confirmed =
            if (globalResponse.totalConfirmed == 0) all - death else globalResponse.totalConfirmed
        return CovidTotal(all, death, confirmed)
    }

    fun listUzbDataToCovidTotal(ls: List<Covid19Response>): CovidTotal {
        val all = ls[ls.lastIndex].confirmed - ls[0].confirmed
        val death = ls[ls.lastIndex].deaths - ls[0].deaths
        val recover = if (ls[ls.lastIndex].recovered - ls[0].recovered == 0) all - death else
            ls[ls.lastIndex].recovered - ls[0].recovered
        return CovidTotal(all, death, recover)
    }

    fun uzbSingleDataToCovidTotal(covid19Response: Covid19Response): CovidTotal {
        val all = covid19Response.confirmed
        val death = covid19Response.deaths
        val confirmed = if (covid19Response.recovered == 0) all - death else covid19Response.recovered
        return CovidTotal(all, death, confirmed)
    }
}
