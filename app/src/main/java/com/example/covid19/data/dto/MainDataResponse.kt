package com.example.covid19.data.dto

import com.google.gson.annotations.SerializedName

data class MainDataResponse(
    @SerializedName("Countries")
    val countryResponses: List<CountryResponse>,
    @SerializedName("Date")
    val date: String,
    @SerializedName("Global")
    val globalResponse: GlobalResponse,
    @SerializedName("ID")
    val id: String,
    @SerializedName("Message")
    val message: String
)