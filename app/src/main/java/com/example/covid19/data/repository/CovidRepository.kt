package com.example.covid19.data.repository

import com.example.covid19.data.api.CovidAPI
import com.example.covid19.data.dto.DataFromToRequest
import com.example.covid19.data.dto.GlobalResponse
import com.example.covid19.data.dto.Covid19Response
import com.example.covid19.domain.repository.Covid
import javax.inject.Inject

class CovidRepository @Inject constructor(private val covidAPI: CovidAPI) : Covid {
    override suspend fun getWorldStatistics(): GlobalResponse {
        return covidAPI.getCountries().globalResponse
    }

    override suspend fun getUzbData(dataRequest: DataFromToRequest): List<Covid19Response> {
        return covidAPI.getUzbData(dataRequest.from, dataRequest.to)
    }

    override suspend fun getUzbDataSingle(): Covid19Response {
        return covidAPI.getUzbSingleData()[covidAPI.getUzbSingleData().lastIndex]
    }
}